import cv2
from tkinter import *
from tkinter import filedialog


class Peb(object):
    def __init__(self):
        self.tela = Tk()
        self.tela.geometry('200x100')
        self.tela['bg'] = 'light blue'

        # criacao do label e botao
        self.txt = Label(self.tela, text = 'Selecione sua imagem: ', bg = 'light blue')
        self.botao1 = Button(self.tela, text = 'Selecionar', command = self.pega_imagem)
        self.confirma = Button(self.tela, text = 'Confirmar', command = self.aplica_filtro)
        self.nome_arq = Label(self.tela, text = '', bg = 'light blue')

        # empacotamento
        self.txt.pack()
        self.botao1.pack()
        self.nome_arq.pack()

        self.tela.mainloop()

    def pega_imagem(self):
        self.img = filedialog.askopenfilename()
        self.nome_arq['text'] = self.img
        self.confirma.pack()

    def aplica_filtro(self):
        img_orig = cv2.imread(self.img)
        img_cinza = cv2.imread(self.img, 0)
        img_cinza_invert = cv2.bitwise_not(img_cinza)

        titles = ['Imagem Original', 'Tons de cinza', 'Cinza invertido']
        imgs = [img_orig, img_cinza, img_cinza_invert]

        cv2.imshow(titles[0], imgs[0])
        cv2.imshow(titles[1], imgs[1])
        cv2.imshow(titles[2], imgs[2])
        cv2.waitKey(0)


Peb()
